package com.example.cargame.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.cargame.R
import com.example.cargame.fragments.GameFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}