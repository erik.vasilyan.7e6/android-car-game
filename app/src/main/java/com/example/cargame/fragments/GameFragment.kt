package com.example.cargame.fragments

import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import com.example.cargame.databinding.FragmentGameBinding
import com.example.cargame.views.GameView

class GameFragment : Fragment() {

    private lateinit var binding: FragmentGameBinding
    private lateinit var gameView: GameView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        gameView = GameView(requireContext(), size)
        binding = FragmentGameBinding.inflate(layoutInflater)
        return gameView
    }
}