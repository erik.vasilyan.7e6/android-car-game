package com.example.cargame.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.cargame.R
import com.example.cargame.databinding.FragmentGameBinding
import com.example.cargame.databinding.FragmentMenuBinding

class MenuFragment : Fragment() {

    private lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.playButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }

        binding.helpButton.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setTitle("Car Race Game")
            alertDialog.setMessage (
                "Car Race Game is a game in which player attempt' to not to hit the cars and get as many gold coins as he can" +
                        "\n\nClick on the left side of the screen to go left." +
                        "\n\nClick on the right side of the screen to go right." +
                        "\n\nGood Luck!")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "OK"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }
    }
}