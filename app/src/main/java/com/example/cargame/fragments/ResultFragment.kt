package com.example.cargame.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.cargame.R
import com.example.cargame.databinding.FragmentResultBinding

class ResultFragment : Fragment() {

    private lateinit var binding: FragmentResultBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentResultBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val playerGoldCoins = arguments?.getString("golds")

        binding.goldCoinsTv.text = playerGoldCoins

        binding.playAgainButton.setOnClickListener {
            findNavController().navigate(R.id.action_resultFragment_to_gameFragment)
        }

        binding.menuButton.setOnClickListener {
            findNavController().navigate(R.id.action_resultFragment_to_menuFragment)
        }

        binding.shareButton.setOnClickListener {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, "I finished Car Race Game and got $playerGoldCoins gold coins")
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
    }
}