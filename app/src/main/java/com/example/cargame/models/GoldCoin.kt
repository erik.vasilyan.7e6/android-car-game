package com.example.cargame.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.cargame.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class GoldCoin(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.gold_coin)
    private val width = screenX / 9f
    private val height = screenY / 16f
    var positionX = listOf(screenX / 7.2, screenX / 3.0, screenX / 1.85, screenX / 1.35).random()
    var positionY = screenY / 100
    var positionGoldCoin = RectF()
    var speed = 0

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionGoldCoin.left = positionX.toFloat()
        positionGoldCoin.right = positionGoldCoin.left + width
    }

    fun updateGoldCoin() {
        positionY += speed
        positionGoldCoin.top = positionY.toFloat()
        positionGoldCoin.bottom = positionGoldCoin.top + height
    }
}

