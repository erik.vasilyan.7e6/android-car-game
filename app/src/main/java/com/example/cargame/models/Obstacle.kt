package com.example.cargame.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.cargame.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Obstacle(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, listOf(R.drawable.car_1, R.drawable.car_2, R.drawable.car_3, R.drawable.car_4, R.drawable.car_5, R.drawable.car_6, R.drawable.car_7, R.drawable.car_8).random())
    private val width = screenX / 7f
    private val height = screenY / 8f
    var positionX = listOf(screenX / 8.0, screenX / 3.1, screenX / 1.9, screenX / 1.375).random()
    var positionY = screenY / 100
    var positionObstacle = RectF()
    var speed = 0

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionObstacle.left = positionX.toFloat()
        positionObstacle.right = positionObstacle.left + width
    }

    fun updateObstacle() {
        positionY += speed
        positionObstacle.top = positionY.toFloat()
        positionObstacle.bottom = positionObstacle.top + height
    }
}