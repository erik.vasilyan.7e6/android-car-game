package com.example.cargame.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.cargame.R

class Player(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.player_car)
    private val width = screenX / 7f
    private val height = screenY / 8f
    var lives = 3
    var goldCoins = 0
    var positionX = screenX / 1.9
    var positionY = screenY / 1.3
    var positionPlayer = RectF()

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionPlayer.top = positionY.toFloat()
        positionPlayer.bottom = positionPlayer.top + height
    }

    fun goLeft() {
        if (positionX > 105) {
            positionX -= 10
        }
    }

    fun goRight() {
        if (positionX < 810) {
            positionX += 10
        }
    }

    fun updatePlayer() {
        positionPlayer.left = positionX.toFloat()
        positionPlayer.right = positionPlayer.left + width
    }
}

