package com.example.cargame.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.navigation.findNavController
import com.example.cargame.R
import com.example.cargame.fragments.GameFragmentDirections
import com.example.cargame.models.GoldCoin
import com.example.cargame.models.Obstacle
import com.example.cargame.models.Player
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.CopyOnWriteArrayList

@SuppressLint("ViewConstructor")
class GameView(context: Context, private val size: Point) : SurfaceView(context) {

    private var canvas: Canvas = Canvas()
    private val paint: Paint = Paint()
    private val player = Player(getContext(), size.x, size.y)
    private var playing = true
    private var speed = 10
    private var roadPosition: Float = 0f
    private var backgroundBitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.road)
    private var lifeBitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.life)
    private val obstacleCars = CopyOnWriteArrayList<Obstacle>()
    private val goldCoins = CopyOnWriteArrayList<GoldCoin>()
    private val backgroundMediaPlayer: MediaPlayer = MediaPlayer.create(context, R.raw.background_music)
    private var goldCoinsMediaPlayer: MediaPlayer = MediaPlayer.create(context, R.raw.gold_coin_sound)
    private val carCrashMediaPlayer: MediaPlayer = MediaPlayer.create(context, R.raw.car_crash_sound)

    init {
        startGame()
        backgroundMediaPlayer.start()
        backgroundMediaPlayer.setOnCompletionListener {
            backgroundMediaPlayer.start()
        }
    }

    private fun startGame(){
        backgroundBitmap = Bitmap.createScaledBitmap(backgroundBitmap, size.x, size.y,false)
        lifeBitmap = Bitmap.createScaledBitmap(lifeBitmap, size.x / 9, size.y / 18,false)

        CoroutineScope(Dispatchers.Main).launch {
            while(playing){
                draw()
                update()
                delay(1)
            }
        }

        CoroutineScope(Dispatchers.Main).launch {
            addGoldCoinsAndObstacles()
        }

        CoroutineScope(Dispatchers.Main).launch {
            increaseSpeed()
        }
    }

    private fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()

            paint.color = Color.YELLOW
            paint.textSize = 60f
            paint.textAlign = Paint.Align.RIGHT
            if (roadPosition > 50) roadPosition = 0f
            else roadPosition += 25
            canvas.drawBitmap(backgroundBitmap, 0f, roadPosition, null)
            canvas.drawText("Coins: ${player.goldCoins}", (size.x - paint.descent()), 75f, paint)
            canvas.drawBitmap(lifeBitmap, 0f, 0f, null)
            canvas.drawText(player.lives.toString(), paint.descent()+175, 90f, paint)

            obstacleCars.iterator().forEach{ obstacleCar ->
                obstacleCar.updateObstacle()
                if (RectF.intersects(obstacleCar.positionObstacle, player.positionPlayer)) {
                    player.lives -= 1
                    carCrashMediaPlayer.start()
                    obstacleCars.remove(obstacleCar)

                    if (player.lives == 0) {
                        playing = false
                        backgroundMediaPlayer.stop()
                        backgroundMediaPlayer.reset()
                        val action = GameFragmentDirections.actionGameFragmentToResultFragment(player.goldCoins.toString())
                        findNavController().navigate(action)
                    }
                }
                if (obstacleCar.positionY > size.y) {
                    obstacleCars.remove(obstacleCar)
                }
                canvas.drawBitmap(obstacleCar.bitmap, obstacleCar.positionX.toFloat(), obstacleCar.positionY.toFloat() , paint)
            }

            goldCoins.iterator().forEach{ goldCoin ->
                var drawGoldCoin = true
                goldCoin.updateGoldCoin()
                if (RectF.intersects(goldCoin.positionGoldCoin, player.positionPlayer)) {
                    player.goldCoins += 1
                    goldCoinsMediaPlayer = MediaPlayer.create(context, R.raw.gold_coin_sound)
                    goldCoinsMediaPlayer.start()
                    goldCoins.remove(goldCoin)
                }
                for (obstacleCar in obstacleCars) {
                    if (RectF.intersects(goldCoin.positionGoldCoin, obstacleCar.positionObstacle)) {
                        drawGoldCoin = false
                        goldCoins.remove(goldCoin)
                        break
                    }
                }
                if (drawGoldCoin) canvas.drawBitmap(goldCoin.bitmap, goldCoin.positionX.toFloat(), goldCoin.positionY.toFloat() , paint)
            }

            canvas.drawBitmap(player.bitmap, player.positionX.toFloat(), player.positionY.toFloat(), paint)

            holder.unlockCanvasAndPost(canvas)
        }
    }

    private fun update() {
        player.updatePlayer()
    }

    private suspend fun addGoldCoinsAndObstacles() {
        while (playing) {
            val obstacle = Obstacle(context, size.x, size.y)
            val goldCoin = GoldCoin(context, size.x, size.y)
            goldCoin.speed = speed
            obstacle.speed = speed
            goldCoins.add(goldCoin)
            obstacleCars.add(obstacle)
            delay(1500)
        }
    }

    private suspend fun increaseSpeed() {
        while (playing) {
            delay(10000)
            speed += 1
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when(event.action){
                // Aquí capturem els events i el codi que volem executar per cadascún
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    // Modifiquem la velocitat del jugador perquè es mogui
                    if (event.x > player.positionX) {
                        player.goRight()
                    }
                    else if (event.x < player.positionX) {
                        player.goLeft()
                    }
                }

            }
        }
        return true
    }
}
